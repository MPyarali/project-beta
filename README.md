# **CarCar Application**

## **Team:**

**Person 1 - Monika: Auto Sales**

**Person 2 - Tyler: Auto Service**

## **Overview**

- The CarCar application is a platform designed to simplify the process of managing inventory, sales and services for a car dealership. It provides features such as an easily accessible form-based structure for updating the inventory with new makes, models and autos and managing customer relations such as the sale of a car or getting a car serviced. With this application, dealerships can easily and efficiently manage thier day-to-day operations.

![Dashboard](Home.png)

## **Design Features**
The CarCar application is a microservice-based application with three main branches:

**Inventory**

**Services**

**Sales**


### **Integration**

The inventory branch stores information about all vehicles in the dealership. This information is then sent out to the service and sales microservices using a poller, which contacts the inventory database to retrieve information about automobiles including thier sold status and vip status. With this integrated functionality, the CarCar app is able to quickly keep track of changes in our inventory - as quick as the click of a button!

### **Inventory microservice**
- The inventory microservice includes the ability to easily add, update and remove manufacturers, models, and vehicles from the inventory. Some of the details tracked by this microservice include the manufacturer, model, year, color, VIN and sold status of each vehicle.

![Dashboard](Auto.png)

Screenshot of form to add a new automobile to the inventory

![Dashboard](AutoList.png)

Screenshot of automobiles listed in the inventory

### **Service microservice**

The service microservice gives one the ability to:

1. add a automotive technician to the database

![alt text](image.png)

2. see a list of all automotive technicians
![alt text](image-1.png)

3. create an automotive service appointment

![alt text](image-2.png)
4. see a list of all active autmotive service appointments
![alt text](image-3.png)
5. see a history of all automotive service appointments
![alt text](image-4.png)

Within the list of active automotive service appointments, the appointment is marked as VIP if the car is in the inventory (meaning if the dealership sold the car to the customer).

There is also a button to set the appointment to canceled or finished, after which, the canceled/finished appointment no longer shows up in the list of appointments, but is still visible within service history.

![alt text](image-5.png)



### **Sales microservice**
- The Sales Microservice encompasses the ability to update information pertaining to vehicle sales, including information about the salesperson, customer, vehicle and price.

Concept model diagram for sales microservice structure:

![Dashboard](SalesDiagram.png)

This microservice allows users to register salespeople and customers and access a respective list with details on each registered salesperson and customer.

The sales form can be used to record a sales transaction by selecting a salesperson, customer and auto from the inventory. Only autos that are currently unsold are available as an option on the sales form.

![Dashboard](SaleForm.png)

This microservice also includes a feature that lists all of the sales for an individual salesperson.

![Dashboard](SalespersonHistory.png)


### **Technologies used**

- Frontend: React.js, HTML, CSS, Bootstrap, Node.js
- Backend: Django
- Database: PostgreSQL
- Deployment: Docker, Docker Compose
- Version Control: Git, GitLab

### **Installation**

1) Fork and clone the repository: https://gitlab.com/MPyarali/project-beta
2) Run Docker:
    - `docker volume create beta-data`
    - `docker-compose build `
    - `docker compose up`
3) Check that all Docker containers are running
  - To troubleshoot containers, try navigating to the container name and checking the logs. You can also run commands from the exec tab on the container.
4) The application should now be running locally on:
    - http://localhost:3000:  CarCar application front-end
    - http://localhost:8100: Inventory API backend
    - http://localhost:8080: Services API backend
    - http://localhost:8090: Sales API backend

If you encounter any issues with installation, please do let us know!

### **Usage**
- At this stage, the application does not require login. You may use the application once it is installed.

### **Contributing**
- This is an open-source database that accepts contributions from all developers! Please follow the steps below if you would like to contribute:
1) Fork the repository
2) Create a new branch: `git checkout -b feature/your-feature-name`
3) Commit changes: `git commit -m "Your message here"`
4) Push to the branch: `git push origin feature/your-feature-name`
5) Create a new Merge request


## **Service Miscroservice CRUD (Create, Read, Update, Delete) Routes**

## Create Technician
- **Endpoint**: `/api/technicians`
- **Method**: POST
- **Description**: Create a new technician.
- **Request Body Example **:
{
	"first_name": "deletemofo",
	"last_name": "now",
	"employee_id": "4"
}
## List Technicians
- **Endpoint**: `/api/technicians`
- **Method**: GET
- **Description**: Get list of technicians.
- **Request Body Example **:
{
	"technicians": [
		{
			"href": "/api/technicians/1/",
			"first_name": "ricky",
			"last_name": "bobby",
			"employee_id": "7"
		},
		{
			"href": "/api/technicians/2/",
			"first_name": "wumbo",
			"last_name": "buchet",
			"employee_id": "100100100"
		}
	]
}

## Create Appointment
- **Endpoint**: `/api/appointments`
- **Method**: POST
- **Description**: Create a new appointment.
- **Request Body Example **:
{
	"date_time": "2012-07-10 14:58:00.000000",
	"reason": "pimpin",
	"status": "riding dirty",
	"vin": "123",
	"customer": "big pimpin",
	"technician": "3"
}
## List Appointments
- **Endpoint**: `/api/appointments`
- **Method**: GET
- **Description**: Get list of appointments.
- **Request Body Example **:
{
	"appointments": [
		{
			"date_time": "2024-03-21T00:00:00+00:00",
			"reason": "on fire",
			"status": "canceled",
			"vin": "100100100",
			"customer": "tyler",
			"id": 1,
			"vip": "false",
			"technician": "ricky"
		},
		{
			"date_time": "2024-03-21T00:00:00+00:00",
			"reason": "on fire",
			"status": "finished",
			"vin": "1C3CC5FB2AN120174",
			"customer": "tyler",
			"id": 3,
			"vip": "false",
			"technician": "ricky"
		},
]}
## Finish Appointment
- **Endpoint**: `/api/appointments/<int:pk>/finish/`
- **Method**: PUT
- **Description**: Get list of appointments.
- **Request Body Example **:
{
	"date_time": "2024-03-20T00:00:00+00:00",
	"reason": "TEST",
	"status": "finished",
	"vin": "1",
	"customer": "tyler",
	"id": 4,
	"vip": false,
	"technician": "fred"
}
## Cancel Appointment
- **Endpoint**: `/api/appointments/<int:pk>/cancel/`
- **Method**: PUT
- **Description**: Get list of appointments.
- **Request Body Example **:
{
	"date_time": "2024-03-20T00:00:00+00:00",
	"reason": "TEST",
	"status": "canceled",
	"vin": "1",
	"customer": "tyler",
	"id": 4,
	"vip": false,
	"technician": "fred"
}


## **Sales Microservice CRUD (Create, Read, Update, Delete) Routes**

## Salesperson

#### Create Salesperson

- **Endpoint**: `/api/salespeople`
- **Method**: POST
- **Description**: Create a new salesperson.
- **Request Body Example **:
{
			"first_name": "Freddy",
			"last_name": "SellsCars",
			"employee_id": "FSellsCars_12"
}

#### List Salespeople

- **Endpoint**: `/api/salespeople`
- **Method**: POST
- **Description**: Retrieve a list of all salespeople registered in the database.

#### Show Salesperson

- **Endpoint**: `/api/salespeople/:id`
- **Method**: GET
- **Description**: Retrieve details of a specific salesperson by ID.


#### Delete Sale

- **Endpoint**: `/api/salespeople/:id`
- **Method**: DELETE
- **Description**: Delete a specific salesperson by ID.


## Customer

#### Create Customer

- **Endpoint**: `/api/customers`
- **Method**: POST
- **Description**: Create a new customer.
- **Request Body Example **:
{
			"first_name": "Reina",
			"last_name": "Delcamino",
			"address": "87 Hill St",
			"phone_number": "123456789"
}

#### Get Customer details

- **Endpoint**: `/api/customers/:id`
- **Method**: GET
- **Description**: Retrieve details of a specific customer by their ID.

#### List Customers

- **Endpoint**: `/api/customers`
- **Method**: GET
- **Description**: Retrieve a list of all customers registered in the database.

#### Delete Customer

- **Endpoint**: `/api/customers/:id`
- **Method**: DELETE
- **Description**: Delete a specific customer by their ID.

## Sales Service

#### Create Sale

- **Endpoint**: `/api/sales`
- **Method**: POST
- **Description**: Create a new sale.
- **Request Body Example**:
{
  "automobile_id": "3",
  "salesperson_id": "1",
  "customer_id": "1",
	"price": 55555
}

#### Get Sale details

- **Endpoint**: `/api/sales/:id`
- **Method**: GET
- **Description**: Retrieve details of a specific sale by its ID.

#### List Sales

- **Endpoint**: `/api/sales/`
- **Method**: GET
- **Description**: Retrieve a list of all sales registered in the database.


#### Delete Sale

- **Endpoint**: `/api/sales/:id`
- **Method**: DELETE
- **Description**: Delete a specific sale by its ID.

## **Inventory Microservice CRUD (Create, Read, Update, Delete) Routes**

### Automobiles Service

#### Create Automobile

- **Endpoint**: `/api/automobiles`
- **Method**: POST
- **Description**: Create a new automobile.
- **Request Body Example **:
{
  "color": "obsidian",
  "year": 2015,
  "vin": "NSSN06FB2AN122012",
  "model_id": 6
}

#### Get Automobile Details

- **Endpoint**: `/api/automobiles/:vin`
- **Method**: GET
- **Description**: Retrieve details of a specific automobile by its VIN number.

#### List Automobiles

- **Endpoint**: `/api/automobiles/`
- **Method**: GET
- **Description**: Retrieve a list of all cars listed in the inventory (regardless of sold status)

#### Update Automobile

- **Endpoint**: `/api/automobiles/:vin`
- **Method**: PUT
- **Description**: Update details of a specific automobile by its VIN number.
- **Request Body Example**:
{
  "color": "space grey",
  "year": 2014,
  "sold": true
}

#### Delete Automobile

- **Endpoint**: `/api/automobiles/:vin`
- **Method**: DELETE
- **Description**: Delete a specific automobile by its VIN number.


### Manufacturers Service

#### Create Manufacturer

- **Endpoint**: `/api/manufacturers`
- **Method**: POST
- **Description**: Create a new manufacturer.
- **Request Body Example**:
{
  "name": "Chrysler"
}

#### Specific Manufacturer Details

- **Endpoint**: `/api/manufacturers/:id`
- **Method**: GET
- **Description**: Retrieve details of a specific manufacturer by its ID.

#### List Manufacturers

- **Endpoint**: `/api/manufacturers/`
- **Method**: GET
- **Description**:  Retrieve a list of all manufacturers listed in the inventory

#### Update Manufacturer

- **Endpoint**: `/api/manufacturers/:id`
- **Method**: PUT
- **Description**: Update details of a specific manufacturer by its ID.
- **Request Body Example**:
{
  "name": "Ford"
}

#### Delete Manufacturer

- **Endpoint**: `/api/manufacturers/:id`
- **Method**: DELETE
- **Description**: Delete a specific manufacturer by its ID.

### Models Service

#### Create Model

- **Endpoint**: `/api/models`
- **Method**: POST
- **Description**: Create a new model.
- **Request Body Example**:
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 6
}

#### Get Model Details

- **Endpoint**: `/api/models/:id`
- **Method**: GET
- **Description**: Retrieve details of a specific model by its ID.

#### List Models

- **Endpoint**: `/api/models/`
- **Method**: GET
- **Description**:  Retrieve a list of all models listed in the inventory

#### Update Model

- **Endpoint**: `/api/models/:id`
- **Method**: PUT
- **Description**: Update details of a specific model by its ID.
- **Request Body Example**:
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}

#### Delete Model

- **Endpoint**: `/api/models/:id`
- **Method**: DELETE
- **Description**: Delete a specific model by its ID.
