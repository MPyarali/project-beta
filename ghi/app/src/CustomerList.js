import React, { useEffect, useState } from 'react';

function CustomerList(){
    const [customer, setCustomer] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setCustomer(data.customer);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    console.log(customer); 

   return (
    <table className="table table-striped">
    <thead>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Address</th>
        <th>Phone Number</th>
      </tr>
    </thead>
    <tbody>
      {customer.map(cst => {
        return (
          <tr key={ cst.id }>
            <td>{ cst.first_name }</td>
            <td>{ cst.last_name }</td>
            <td>{ cst.address }</td>
            <td>{ cst.phone_number }</td> 
          </tr>
        );
      })}
    </tbody>
  </table>
   );
}

export default CustomerList; 
