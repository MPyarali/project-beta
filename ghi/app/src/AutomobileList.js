import React, { useEffect, useState } from 'react';

function AutomobileList(){
    const [autos, setAutos] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setAutos(data.autos);
        }

    }
    useEffect(() => {
        fetchData();
    }, [])
  

   return (
    <table className="table table-striped">
    <thead>
      <tr>
        <th>VIN</th>
        <th>color</th>
        <th>year</th>
        <th>model</th>
        <th>manufacturer</th>
        <th>sold</th>
      </tr>
    </thead>
    <tbody>
      {autos.map(auto => {
        return (
          <tr key={ auto.id }>
            <td>{ auto.vin }</td>
            <td>{ auto.color }</td>
            <td>{ auto.year }</td>
            <td>{ auto.model.name }</td>
            <td>{ auto.model.manufacturer.name }</td>
            <td>{ auto.sold }</td>
          </tr>
        );
      })}
    </tbody>
  </table>
   );
}

export default AutomobileList;
