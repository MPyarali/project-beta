import React, { useEffect, useState } from 'react';

function SalespersonList(){
    const [salesperson, setSalesperson] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setSalesperson(data.salesperson);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    console.log(salesperson); 

   return (
    <table className="table table-striped">
    <thead>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Employee ID</th>
      </tr>
    </thead>
    <tbody>
      {salesperson.map(employee => {
        return (
          <tr key={ employee.id }>
            <td>{ employee.first_name }</td>
            <td>{ employee.last_name }</td>
            <td>{ employee.employee_id }</td>
          </tr>
        );
      })}
    </tbody>
  </table>
   );
}

export default SalespersonList; 