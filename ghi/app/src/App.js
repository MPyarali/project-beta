import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechForm from './TechForm';
import TechList from './TechList';
import ServiceForm from './ServiceForm';
import ServiceHistory from './ServiceHistory';
import ServiceAppointments from './ServiceAppointments';
import SalespersonList from './SalespersonList';
import SalespersonForm from './SalespersonForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalespersonHistory from './SalespersonHistory';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VehicleList from './VehicleList';
import VehicleForm from './VehicleForm';
import AutomobileForm  from './AutomobileForm';

import AutomobileList from './AutomobileList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians/" element={<TechList />} />
          <Route path="/technicians/create/" element={<TechForm />} />
          <Route path="/appointments/create/" element={<ServiceForm />} />
          <Route path="/salespeople/" element={<SalespersonList />} />
          <Route path="/salespeople/create" element={<SalespersonForm />} />
          <Route path="/customers/" element={<CustomerList />} />
          <Route path="/customers/create" element={<CustomerForm />} />
          <Route path="/sales/" element={<SalesList />} />
          <Route path="/sales/create" element={<SalesForm />} />
          <Route path="/salespeople/history" element={<SalespersonHistory />} />
          <Route path="/manufacturers/create" element={<ManufacturerForm />} />
          <Route path="/manufacturers/" element={<ManufacturerList />} />
          <Route path="/models/create" element={<VehicleForm />} />
          <Route path="/models/" element={<VehicleList />} />
          <Route path="/automobiles/create" element={<AutomobileForm />} />
          <Route path="/appointments/history/" element={<ServiceHistory />} />
          <Route path="/appointments/" element={<ServiceAppointments />} />
          <Route path="/automobiles/" element={<AutomobileList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
