import React, {useState, useEffect } from 'react';

function AutomobileForm() {
  const [models, setModels] = useState([])
  const [formData, setFormData] = useState({
      color: '',
      year:'',
      vin:'',
      model_id: '',
  })


  const fetchModelData = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);
    if (response.ok){
        const data = await response.json();
        setModels(data.models);
    }
}

useEffect(() => {
    fetchModelData();
}, []);


  const [hasSignedUp, setHasSignedUp] = useState(false)
  const formClasses = (!hasSignedUp) ? '' : 'd-none';
  const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';


  const handleSubmit = async (event) => {
      event.preventDefault();

      const url = 'http://localhost:8100/api/automobiles/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
          setFormData({
            color: '',
            year:'',
            vin:'',
            model_id: '',
          });
          setHasSignedUp(true);
        }
      }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        ...formData,
        [inputName]: value
        });
    }


      return (
        <div className="my-5">
        <div className="row">
          <div className="col-md-8 mx-auto">
            <div className="col">
                <div className="card shadow">
                <div className="card-body">
                    <img
                    width="200"
                    className="bg-white rounded shadow d-block mx-auto mb-4"
                    src="https://cdn.pixabay.com/photo/2012/04/13/20/37/car-33556_1280.png"
                    alt='Customers'
                    />

                    <form className={formClasses} onSubmit={handleSubmit} id="create-customer-form">
                    <h1 className="card-title text-center">Enter a New Automobile</h1>
                    <p className="mb-3 text-center">
                    Please add auto details below
                    </p>
                    <div className="row">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.color} required placeholder="color" type="text" id="color" name="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.year} required placeholder="year" type="text" id="year" name="year" className="form-control" />
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.vin} required placeholder="vin" type="text" id="vin" name="vin" className="form-control" />
                            <label htmlFor="vin">VIN #</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.model_id}required name="model_id" id="model_id" className="form-select">
                                <option value="">Choose a model</option>
                                {models.map(model => {
                                return (
                                    <option key={model.id} value={model.id}>{model.manufacturer.name} {model.name}</option>
                                )
                                })}
                            </select>
                        </div>
                    <button className="btn btn-lg btn-primary">Add Automobile</button>
                    </div>
                    </form>
                    <div className='text-center'>
                        <div className={messageClasses} id="success-message">
                        <h2 className="mb-1"> Wow! This one is going to sell fast! </h2>
                        <button onClick ={ () => setHasSignedUp(false)} className="btn btn-lg btn-secondary mb-3">
                            Add another automobile
                        </button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      );

}
export default AutomobileForm;
