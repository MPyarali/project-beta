import React, {useState, useEffect } from 'react';

function ManufacturerForm() {
  const [formData, setFormData] = useState({
      name: '',
  })

  const [hasSignedUp, setHasSignedUp] = useState(false)
  const formClasses = (!hasSignedUp) ? '' : 'd-none';
  const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';


  const handleSubmit = async (event) => {
      event.preventDefault();

      const url = 'http://localhost:8100/api/manufacturers/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
          setFormData({
              name: '',
          });
          setHasSignedUp(true);
        }
      }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        ...formData,
        [inputName]: value
        });
    }


      return (
        <div className="my-5">
        <div className="row">
          <div className="col-md-8 mx-auto">
            <div className="col">
                <div className="card shadow">
                <div className="card-body">
                    <img
                    width="100"
                    className="bg-white rounded shadow d-block mx-auto mb-4"
                    src="https://cdn.pixabay.com/photo/2021/03/30/09/19/factory-6136460_1280.png"
                    alt='Salesperson'
                    />

                    <form className={formClasses} onSubmit={handleSubmit} id="create-customer-form">
                    <h1 className="card-title text-center">Enter a New Manufacturer</h1>
                    <div className="row">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.name} required placeholder="name" type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="name">Manufacturer Name</label>
                        </div>
                    <button className="btn btn-lg btn-primary">Add Manufacturer</button>
                    </div> 
                    </form>
                    <div className='text-center'>
                        <div className={messageClasses} id="success-message">
                        <h3 className="mb-4"> Nice job! </h3>                
                        <button onClick ={ () => setHasSignedUp(false)} className="btn btn-lg btn-secondary mb-3">
                            Enter another manufacturer
                        </button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
          </div> 
        </div>
      </div>
      );

}
export default ManufacturerForm;