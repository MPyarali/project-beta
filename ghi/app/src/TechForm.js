import React, {useState, useEffect } from 'react';

function TechForm() {
  const [formData, setFormData] = useState({
      first_name: '',
      last_name:'',
      employee_id:'',
  })
  console.log("formData", formData)

  const [hasSignedUp, setHasSignedUp] = useState(false)


  const handleSubmit = async (event) => {
      event.preventDefault();

      const techUrl = 'http://localhost:8080/api/technicians/';
      console.log("techUrl", techUrl)

      const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(techUrl, fetchConfig);
        console.log("response", response)

        if (response.ok) {
          setFormData({
              first_name: '',
              last_name:'',
              employee_id:'',
          });
          setHasSignedUp(true);
        }
      }

        const handleChangeName = (e) => {
          const value = e.target.value;
          const inputName = e.target.name;
          setFormData({
            ...formData,
            [inputName]: value
          });
        }

      const formClasses = (!hasSignedUp) ? '' : 'd-none';
      const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';


      return (
          <div className="my-5">
        <div className="row">
          <div className="col col-sm-auto">

          </div>

          <div className="col">
            <div className="card shadow">
              <div className="card-body">

                <form className={formClasses} onSubmit={handleSubmit} id="create-tech-form">
                  <h1 className="card-title">add a technician</h1>
                  <p className="mb-3">
                    please create technician details
                  </p>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleChangeName} value={formData.first_name} required placeholder="first_name" type="text" id="first_name" name="first_name" className="form-control" />
                        <label htmlFor="first_name">first name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleChangeName} value={formData.last_name}required placeholder="last_name" type="text" id="last_name" name="last_name" className="form-control" />
                        <label htmlFor="last_name">last name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleChangeName} value={formData.employee_id}required placeholder="employee_id" type="number" id="employee_id" name="employee_id" className="form-control" />
                        <label htmlFor="employee_id">employee id</label>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">add technician</button>
                </form>

                <div className={messageClasses} id="success-message">
                  good job, you made a technician, you must be a proud parent
                  <button onClick ={ () => setHasSignedUp(false)}>
                    make another technician
                  </button>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      );

}
export default TechForm;
