import React, {useState, useEffect } from 'react';

function ServiceForm() {
  const [technicians, setTechnicians] = useState([])
  const [formData, setFormData] = useState({
    date_time: '',
    reason: '',
    status: '',
    vin: '',
    customer:  '',
    technician: ''
  })

  const [hasSignedUp, setHasSignedUp] = useState(false)

  const getData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
      event.preventDefault();

      const apptUrl = `http://localhost:8080/api/appointments/`;
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
              'Content-Type': 'application/json',
            },
        };

        const response = await fetch(apptUrl, fetchConfig);
        if (response.ok) {
          const appointmentData = await response.json();
          const vinUrl = `http://localhost:8100/api/automobiles/${formData.vin}`
          console.log("vinURl", vinUrl)
          const vinResponse = await fetch(vinUrl);

          if (vinResponse.ok) {
            const vinData = await vinResponse.json();

              const updateUrl = `http://localhost:8080/api/appointments/${appointmentData.id}/`;
              console.log(updateUrl)
              const updatedData = {
                ...formData,
                vip: "YES"
              }
              const updateConfig = {
                method: "put",
                body: JSON.stringify(updatedData),
                headers: {
                  'Content-Type': 'application/json',
                }
              };
              const updateResponse = await fetch(updateUrl, updateConfig);
              if (!updateResponse.ok) {
                console.error("failed to update appointment to VIP", await updateResponse.text())
              }
          }
          else {
            console.error("vin number not in inventory")
          }
          setFormData({
            date_time: '',
            reason: '',
            status: '',
            vin: '',
            customer:  '',
            technician: '',
          });

            setHasSignedUp(true);
        } else {
          console.error("failed to create appointment")
        }
    }
    const handleChangeName = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    const formClasses = (!hasSignedUp) ? '' : 'd-none';
    const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

    return (
        <div className="my-5">
          <div className="row">
            <div className="col col-sm-auto">

            </div>

            <div className="col">
              <div className="card shadow">
                <div className="card-body">

                  <form className={formClasses} onSubmit={handleSubmit} id="create-appointment-form">
                    <h1 className="card-title">create a service appointment</h1>
                    <p className="mb-3">
                      please add appointment details
                    </p>

                    <div className="mb-3">
                      <select onChange={handleChangeName}value = {formData.technician} name="technician" id="technician" required>
                        <option >select technician</option>
                        {
                          technicians.map(technician => {
                            return (
                              <option key={technician.href} value={technician.employee_id}>{technician.first_name}</option>
                            )
                          })
                        }
                      </select>
                    </div>

                    <div className="row">
                      <div className="col">
                        <div className="form-floating mb-3">
                          <input onChange={handleChangeName} value = {formData.date_time} required placeholder="date_time" type="date" id="date_time" name="date_time" className="form-control" />
                          <label htmlFor="date_time">select a date and time</label>
                        </div>
                      </div>
                      <div className="col">
                        <div className="form-floating mb-3">
                          <input onChange={handleChangeName}value = {formData.reason} required placeholder="reason" type="text" id="reason" name="reason" className="form-control" />
                          <label htmlFor="reason">what is the reason for your appointment?</label>
                        </div>
                      </div>
                      <div className="col">
                        <div className="form-floating mb-3">
                          <input onChange={handleChangeName} value = {formData.vin} required placeholder="vin" type="text" id="color" name="vin" className="form-control" />
                          <label htmlFor="vin">enter your VIN number</label>
                        </div>
                      </div>
                      <div className="col">
                        <div className="form-floating mb-3">
                          <input onChange={handleChangeName} value = {formData.customer} required placeholder="customer" type="text" id="customer" name="customer" className="form-control" />
                          <label htmlFor="customer">enter your name</label>
                        </div>
                      </div>
                    </div>
                    <button className="btn btn-lg btn-primary">create an appointment</button>
                  </form>

                  <div className={messageClasses} id="success-message">
                    good job, you made an appointment, now it's time to lower your expectations
                    <button onClick ={ () => setHasSignedUp(false)}>
                      give us more money please (add another appointment)
                    </button>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      );

}



    export default ServiceForm
