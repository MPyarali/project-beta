import React, { useEffect, useState } from 'react';

function VehicleModelList(){
    const [models, setModels] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setModels(data.models);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    console.log(models); 

   return (
    <table className="table table-striped">
    <thead>
      <tr>
        <th>Manufacturer</th>
        <th>Name</th>
        <th>Picture</th>
      </tr>
    </thead>
    <tbody>
      {models.map(model => {
        return (
          <tr key={ model.id }>
            <td>{ model.manufacturer.name }</td>
            <td>{ model.name }</td>
            <td><img src={ model.picture_url } alt="car_model" length="200" width="200"/></td>
          </tr>
        );
      })}
    </tbody>
  </table>
   );
}

export default VehicleModelList; 
