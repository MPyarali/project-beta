import React, { useEffect, useState } from 'react';

function SalespersonHistory(){
    const [sales, setSales] = useState([]);
    const [filteredSales, setFilteredSales] = useState([]); 
    const [selectedSalesperson, setSelectedSalesperson] = useState(''); 
    const[salesperson, setSalesperson] = useState([]); 



    const fetchData = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setSales(data.sales);
            setFilteredSales(data.sales); 
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

    console.log(sales); 

    const fetchSalespersonData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url); 
        if (response.ok){
            const data = await response.json(); 
            setSalesperson(data.salesperson); 
        } 
    }
    
    useEffect(() => {
        fetchSalespersonData(); 
    }, []); 

    useEffect(() => {
        if(selectedSalesperson) {
            const filteredData = sales.filter(sale => sale.salesperson.employee_id === selectedSalesperson.employee_id); 
            setFilteredSales(filteredData); 
        } else {
            setFilteredSales(sales); 
        }
    }, [selectedSalesperson, sales]); 

    console.log(filteredSales); 

   return (
    <div>
      <select
        onChange={(e) => setSelectedSalesperson(salesperson.find(employee => employee.id === parseInt(e.target.value)))}
        className="form-control" 
        style={{ width: '100%', fontSize: '20px', marginTop: '30px', marginBottom: '40px' }} 
        >        
        <option value="">Select a Salesperson</option>
        {salesperson.map(employee => {
            return (
                <option key={employee.id} value={employee.id}>{employee.first_name} {employee.last_name}</option>
            )
            })}
      </select>
    <table className="table table-striped">
    <thead>
      <tr>
        <th>Salesperson Employee ID</th>
        <th>Salesperson Name</th>
        <th>Customer</th>
        <th>VIN</th>
        <th>Price</th>
      </tr>
    </thead>
    <tbody>
      {filteredSales.map(sale => {
        return (
          <tr key={ sale.id }>
            <td>{ sale.salesperson.employee_id }</td>
            <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
            <td>{ sale.customer.first_name } { sale.customer.last_name}</td>
            <td>{ sale.automobile.vin } </td>
            <td>{ sale.price }</td>
          </tr>
        );
      })}
    </tbody>
  </table>
  </div>
   );
}

export default SalespersonHistory; 