import { useEffect, useState } from 'react';

function ServiceAppointments() {
  const [appointments, setAppointments] = useState([]);


  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const data = await response.json();
      const filteredAppointments = data.appointments.filter(item => item.status !== "finished" && item.status !== "canceled");
        setAppointments(filteredAppointments)
      }
    }
    useEffect(()=>{
      getData()
    }, [])

    const updateStatusFinish = async (id) => {
      const url = `http://localhost:8080/api/appointments/${id}/finish`;
      const fetchConfig = {
        method: 'put',
        headers: {
            'Content-Type' : 'application/json',
        },
        body: JSON.stringify({})
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      getData();
    } else {
      console.error('http error:', response.status);
    }

    };

    const updateStatusCancel = async (id) => {
      const url = `http://localhost:8080/api/appointments/${id}/cancel`;
      const fetchConfig = {
        method: 'put',
        headers: {
            'Content-Type' : 'application/json',
        },
        body: JSON.stringify({})
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      getData();
    } else {
      console.error('http error:', response.status);
    }

    };







  return (
    <>
    &nbsp;
    <table className="table table-striped">
      <thead>
        <tr>
          <th>date</th>
          <th>reason</th>
          <th>VIN</th>
          <th>customer</th>
          <th>appt id</th>
          <th>technician</th>
          <th>status</th>
          <th>VIP?</th>
          <th>update status</th>
        </tr>
      </thead>
      <tbody>
        {appointments.map(appointment => {
          return (
            <tr
            key={appointment.id}>
              <td>{ appointment.date_time }</td>
              <td>{ appointment.reason }</td>
              <td>{ appointment.vin }</td>
              <td>{ appointment.customer }</td>
              <td>{ appointment.id }</td>
              <td>{ appointment.technician }</td>
              <td>active</td>
              <td>{ appointment.vip }</td>
              <td>
                  <button onClick={() => updateStatusFinish(appointment.id)}>
                    finish
                  </button>
                  <button onClick={() => updateStatusCancel(appointment.id)}>
                    cancel
                  </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </>
  );
}

export default ServiceAppointments;
