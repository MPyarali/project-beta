import React, {useState, useEffect } from 'react';

function CustomerForm() {
  const [formData, setFormData] = useState({
      first_name: '',
      last_name:'',
      address:'',
      phone_number:'', 
  })

  const [hasSignedUp, setHasSignedUp] = useState(false)
  const formClasses = (!hasSignedUp) ? '' : 'd-none';
  const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';


  const handleSubmit = async (event) => {
      event.preventDefault();

      const url = 'http://localhost:8090/api/customers/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
          setFormData({
              first_name: '',
              last_name:'',
              address: '',
              phone_number: '', 
          });
          setHasSignedUp(true);
        }
      }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        ...formData,
        [inputName]: value
        });
    }

      return (
        <div className="my-5">
        <div className="row">
          <div className="col-md-8 mx-auto">
            <div className="col">
                <div className="card shadow">
                <div className="card-body">
                    <img
                    width="400"
                    className="bg-white rounded shadow d-block mx-auto mb-4"
                    src="https://cdn.pixabay.com/photo/2017/03/19/03/40/avatar-2155431_1280.png"
                    alt='Customers'
                    />

                    <form className={formClasses} onSubmit={handleSubmit} id="create-customer-form">
                    <h1 className="card-title text-center">Create a New Customer</h1>
                    <p className="mb-3 text-center">
                    Please add customer details below 
                    </p>
                    <div className="row">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.first_name} required placeholder="first_name" type="text" id="first_name" name="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.last_name} required placeholder="last_name" type="text" id="last_name" name="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.address} required placeholder="address" type="text" id="address" name="address" className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.phone_number} required placeholder="phone_number" type="text" id="phone_number" name="phone_number" className="form-control" />
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                    <button className="btn btn-lg btn-primary">Add Customer</button>
                    </div> 
                    </form>
                    <div className='text-center'>
                        <div className={messageClasses} id="success-message">
                        <h2 className="mb-1"> You brought in a new customer!</h2>  
                        <h2 className="mb-4">Fantastic job!</h2>              
                        <button onClick ={ () => setHasSignedUp(false)} className="btn btn-lg btn-secondary mb-3">
                            Create a new customer 
                        </button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
          </div> 
        </div>
      </div>
      );

}
export default CustomerForm;