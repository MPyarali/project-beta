import React, {useState, useEffect } from 'react';

function SalespersonForm() {
  const [formData, setFormData] = useState({
      first_name: '',
      last_name:'',
      employee_id:'',
  })

  const [hasSignedUp, setHasSignedUp] = useState(false)
  const formClasses = (!hasSignedUp) ? '' : 'd-none';
  const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';


  const handleSubmit = async (event) => {
      event.preventDefault();

      const url = 'http://localhost:8090/api/salespeople/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
          setFormData({
              first_name: '',
              last_name:'',
              employee_id: '',
          });
          setHasSignedUp(true);
        }
      }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        ...formData,
        [inputName]: value
        });
    }


      return (
        <div className="my-5">
        <div className="row">
          <div className="col-md-8 mx-auto">
            <div className="col">
                <div className="card shadow">
                <div className="card-body">
                    <img
                    width="400"
                    className="bg-white rounded shadow d-block mx-auto mb-4"
                    src="https://cdn.pixabay.com/photo/2016/03/31/20/37/client-1295901_1280.png"
                    alt='Salesperson'
                    />

                    <form className={formClasses} onSubmit={handleSubmit} id="create-customer-form">
                    <h1 className="card-title text-center">Enter a New Salesperson</h1>
                    <p className="mb-3 text-center">
                    Please add salesperson details below 
                    </p>
                    <div className="row">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.first_name} required placeholder="first_name" type="text" id="first_name" name="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.last_name} required placeholder="last_name" type="text" id="last_name" name="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.employee_id} required placeholder="employee_id" type="text" id="employee_id" name="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                    <button className="btn btn-lg btn-primary">Add Salesperson</button>
                    </div> 
                    </form>
                    <div className='text-center'>
                        <div className={messageClasses} id="success-message">
                        <h3 className="mb-4"> Excellent! Welcome to the team!</h3>                
                        <button onClick ={ () => setHasSignedUp(false)} className="btn btn-lg btn-secondary mb-3">
                            Enter another new salesperson
                        </button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
          </div> 
        </div>
      </div>
      );

}
export default SalespersonForm;