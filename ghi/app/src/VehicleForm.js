import React, {useState, useEffect } from 'react';

function VehicleModelForm() {
  const [manufacturers, setManufacturers] = useState([])
  const [formData, setFormData] = useState({
      name: '',
      picture_url:'',
      manufacturer_id:'',
  })


  const fetchManuData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url); 
    if (response.ok){
        const data = await response.json(); 
        setManufacturers(data.manufacturers); 
    } 
}

useEffect(() => {
    fetchManuData(); 
}, []); 


  const [hasSignedUp, setHasSignedUp] = useState(false)
  const formClasses = (!hasSignedUp) ? '' : 'd-none';
  const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';


  const handleSubmit = async (event) => {
      event.preventDefault();

      const url = 'http://localhost:8100/api/models/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
          setFormData({
              name: '',
              picture_url:'',
              manufacturer_id: '',
          });
          setHasSignedUp(true);
        }
      }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        ...formData,
        [inputName]: value
        });
    }


      return (
        <div className="my-5">
        <div className="row">
          <div className="col-md-8 mx-auto">
            <div className="col">
                <div className="card shadow">
                <div className="card-body">
                    <img
                    width="200"
                    className="bg-white rounded shadow d-block mx-auto mb-4"
                    src="https://cdn.pixabay.com/photo/2022/07/31/19/51/car-7356692_1280.png"
                    alt='Customers'
                    />

                    <form className={formClasses} onSubmit={handleSubmit} id="create-customer-form">
                    <h1 className="card-title text-center">Create a New Vehicle Model</h1>
                    <p className="mb-3 text-center">
                    Please add vehicle model details below 
                    </p>
                    <div className="row">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.name} required placeholder="name" type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.picture_url} required placeholder="picture_url" type="text" id="picture_url" name="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.manufacturer_id}required name="manufacturer_id" id="manufacturer_id" className="form-select">
                                <option value="">Choose a manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                return (
                                    <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                )
                                })}
                            </select>
                        </div>
                    <button className="btn btn-lg btn-primary">Add Vehicle Model</button>
                    </div> 
                    </form>
                    <div className='text-center'>
                        <div className={messageClasses} id="success-message">
                        <h2 className="mb-1"> That's a nice car! </h2>           
                        <button onClick ={ () => setHasSignedUp(false)} className="btn btn-lg btn-secondary mb-3">
                            Add another vehicle model 
                        </button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
          </div> 
        </div>
      </div>
      );

}
export default VehicleModelForm;