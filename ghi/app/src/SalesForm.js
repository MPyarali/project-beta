import React, {useState, useEffect } from 'react';

function SalesForm() {
  const[autos, setAutos] = useState([]);
  const[salesperson, setSalesperson] = useState([]); 
  const[customer, setCustomer] = useState([]); 


  const [formData, setFormData] = useState({
      automobile_id: '',
      salesperson_id:'',
      customer_id:'',
      price:'', 
  })

const fetchAutoData = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url); 
    if (response.ok){
        const data = await response.json(); 
        const filteredAutos = data.autos.filter(auto => auto.sold !== true); 
        console.log("filtered_autos", filteredAutos)
        setAutos(filteredAutos); 
    } 
}

useEffect(() => {
    fetchAutoData(); 
}, []); 

const fetchSalespersonData = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url); 
    if (response.ok){
        const data = await response.json(); 
        setSalesperson(data.salesperson); 
    } 
}

useEffect(() => {
    fetchSalespersonData(); 
}, []); 

const fetchCustomerData = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url); 
    if (response.ok){
        const data = await response.json(); 
        setCustomer(data.customer); 
    } 
}

useEffect(() => {
    fetchCustomerData(); 
}, []); 

  const [hasSignedUp, setHasSignedUp] = useState(false)
  const formClasses = (!hasSignedUp) ? '' : 'd-none';
  const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

  const updateSale = async (vin) => {
    const url = `http://localhost:8100/api/automobiles/${vin}/`; 
    
    console.log("VIN", vin)
    const updateConfig = {
        method: 'put',
        body: JSON.stringify({ 
            color: autos.color, 
            year: autos.year, 
            sold: true                   
        }),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(url, updateConfig); 
    if (response.ok) {
        fetchAutoData()
    } else {
        console.error('http error', response.status);
    }
};

  const handleSubmit = async (event) => {
      event.preventDefault();

      const url = 'http://localhost:8090/api/sales/';
      console.log(formData)
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
          setHasSignedUp(true);

          let vin = null

          for (let auto of autos) {
            if (auto.id == formData.automobile_id) {
                vin = auto.vin
            }
          }

          if (vin != null) {
            updateSale(vin)
          } else {
            console.log("No automobile found with id ", formData.automobile_id)
          }

          setFormData({
            automobile_id: '',
            salesperson_id:'',
            customer_id: '',
            price: '', 
        });
        }
      }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
        ...formData,
        [inputName]: value
        });
    }


      return (
        <div className="my-5">
        <div className="row">
          <div className="col-md-8 mx-auto">
            <div className="col">
                <div className="card shadow">
                <div className="card-body">
                    <img
                    width="400"
                    className="bg-white rounded shadow d-block mx-auto mb-4"
                    src="https://cdn.pixabay.com/photo/2018/09/14/16/42/shake-hand-3677534_1280.jpg"
                    alt='New Car'
                    />

                    <form className={formClasses} onSubmit={handleSubmit} id="create-sale-form">
                    <h1 className="card-title text-center">Create a New Sale</h1>
                    <p className="mb-3 text-center">
                    Please add details of sale
                    </p>
                    <div className="row">
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.automobile_id} required name="automobile_id" id="automobile_id" className="form-select">
                                <option value="">Choose a vin</option>
                                {autos.map(auto => {
                                return (
                                    <option key={auto.id} value={auto.id}>{auto.vin}</option>
                                )
                                })}
                            </select>
                        </div>
                        <div className="row">
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.salesperson_id} required name="salesperson_id" id="salesperson_id" className="form-select">
                                <option value="">Choose a salesperson</option>
                                {salesperson.map(employee => {
                                return (
                                    <option key={employee.id} value={employee.id}>{employee.first_name} {employee.last_name}</option>
                                )
                                })}
                            </select>
                        </div>
                        </div> 
                        <div className="row">
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.customer_id} required name="customer_id" id="customer_id" className="form-select">
                                <option value="">Choose a customer</option>
                                {customer.map(buyer => {
                                return (
                                    <option key={buyer.id} value={buyer.id}>{buyer.first_name} {buyer.last_name}</option>
                                )
                                })}
                            </select>
                        </div>
                        </div> 
                        <div className="form-floating mb-3 ml-3">
                            <input onChange={handleFormChange} value={formData.price} required placeholder="price" type="text" id="price" name="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                    <button className="btn btn-lg btn-primary">Add Sale</button>
                    </div> 
                    </form>
                    <div className='text-center'>
                        <div className={messageClasses} id="success-message">
                        <h2 className="mb-1"> Congrats on the new car!</h2>  
                        <h2 className="mb-4">Fantastic choice!</h2>              
                        <button onClick ={ () => setHasSignedUp(false)} className="btn btn-lg btn-secondary mb-3">
                            Create a new sale
                        </button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
          </div> 
        </div>
      </div>
      );

}
export default SalesForm;
