import { useEffect, useState } from 'react';

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [filteredResults, setFilteredResults] = useState([]);
  const [searchInput, setSearchInput] = useState('');

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments)
    }
  }

  const searchItems = (searchValue) => {
      setSearchInput(searchValue)
      if (searchInput.length !== 0) {
        const filteredData = appointments.filter ((item) => {
          return item.vin.toLowerCase().includes(searchInput.toLowerCase())
        })
        setFilteredResults(filteredData)
      }
      else {
        setFilteredResults([]);;
      }
      }

  useEffect(()=>{
    getData()
  }, [])




  return (
    <>
    <div>

      <input icon='search'
                placeholder='Search VIN number...'
                onChange={(e) => searchItems(e.target.value)}
            />
            <button>search VIN number</button>

    </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>date</th>
          <th>reason</th>
          <th>VIN</th>
          <th>customer</th>
          <th>appt id</th>
          <th>technician</th>
          <th>status</th>

        </tr>
      </thead>
      <tbody>
        {searchInput.length === 0 ?
        appointments.map((appointment) => {
          return (
            <tr key={appointment.id}>
              <td>{ appointment.date_time }</td>
              <td>{ appointment.reason }</td>
              <td>{ appointment.vin }</td>
              <td>{ appointment.customer }</td>
              <td>{ appointment.id }</td>
              <td>{ appointment.technician }</td>
              <td>{ appointment.status }</td>
            </tr>
          );
        })
        : filteredResults.map(appointment => {
          return (
            <tr
            key={appointment.id}>
              <td>{ appointment.date_time }</td>
              <td>{ appointment.reason }</td>
              <td>{ appointment.vin }</td>
              <td>{ appointment.customer }</td>
              <td>{ appointment.id }</td>
              <td>{ appointment.technician }</td>
              <td>{ appointment.status }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </>
  );
}

export default ServiceHistory;
