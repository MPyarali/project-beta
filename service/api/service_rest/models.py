from django.db import models
from django.urls import reverse

# A Technician model containing first_name, last_name,
# and employee_id fields.

# An AutomobileVO model containing vin and sold fields.

# An Appointment model containing date_time, reason, status,
# vin, customer and technician fields.


# The technician field should be a foreign key.
# Your vin field should be of type CharField. (It should not be a AutomobileVO foreign key.)


class Technician(models.Model):
    first_name = models.CharField(max_length = 50)
    last_name= models.CharField(max_length = 100)
    employee_id= models.CharField(max_length = 100)

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=50)
    sold = models.CharField(max_length=50)

class Appointment(models.Model):
    date_time = models.DateTimeField(null=True)
    reason = models.CharField(max_length = 500)
    status = models.CharField(max_length=50)
    vin = models.CharField(max_length=50)
    customer = models.CharField(max_length=100)
    vip = models.CharField(max_length=50)
    technician = models.ForeignKey(
        Technician, #if not working could try a different model here 🤔
        related_name="appointments",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name
