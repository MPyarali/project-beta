from django.urls import path

from .views import (
    api_cancel_appointment,
    api_show_appointment,
    api_finish_appointment,
    api_list_appointments,
    api_list_technicians,
    api_show_technician,
)

urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"), #use for list and create tech
     path(
        "technicians/<int:pk>/", #use for delete tech...if this url does not work may need to do id instead of pk
        api_show_technician,
        name="api_show_technician",
    ),
    path("appointments/", api_list_appointments, name="api_list_appointments"), #list all and create an appt
    path("appointments/<int:pk>/", api_show_appointment, name="api_delete_appointment"),
    path("appointments/<int:pk>/cancel", api_cancel_appointment, name="api_cancel_appointment"), #iffy views.py lol...🛑
    path("appointments/<int:pk>/finish", api_finish_appointment, name="api_finish_appointment"), #iffy views.py lol...🛑

]
