from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment


# make technician encoder

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]

# make autoVO encoder

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

# make appt encoder

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "id",
        "vip",
    ]

    def get_extra_data(self, o):
        return {"technician": o.technician.first_name,
                }


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET": #GET getting a list of all TECHS
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else: #use POST method to create TECH 🛑 IS THIS ALL I NEED TO CREATE A TECH?🛑
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_show_technician(request,pk):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET": #List of ALL appointments
        appointments = Appointment.objects.all()
        #if vin is in inventory
        #   set vip == True
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else: #use POST method to create an appointment with a technician
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(employee_id = content["technician"])
            content["technician"]=technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "invalid technician employee_id"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointment(request,pk):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    else: #update with PUT
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(id=pk)

            props = ["vip"]
            for prop in props:
                if prop in content:
                    setattr(appointment, prop, content[prop])

            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,

        )

        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician"},
                status=400,
            )



@require_http_methods(["PUT"])
def api_cancel_appointment(request,pk):
    if request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician = Technician.objects.get(first_name = content["technician"])
                content["technician"] = technician
                # not sure about this .get parameters ^ 🛑
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "invalid technician employee id"},
                status=400
            )
        content["status"] = "canceled"
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)

        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["PUT"])
def api_finish_appointment(request,pk):
    if request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician = Technician.objects.get(first_name = content["technician"])
                content["technician"] = technician
                # not sure about this .get parameters ^ 🛑
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "invalid technician employee id"},
                status=400
            )
        content["status"] = "finished"
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)

        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


#special function for the value object
def api_list_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            automobiles,
            encoder=AutomobileVOEncoder,
            safe=False,
        )
