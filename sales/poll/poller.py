import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO

def poll():
    while True:
        print('Sales poller polling for data')
        try:
            # Write your polling logic, here
            # Do not copy entire file
            url = 'http://project-beta-inventory-api-1:8000/api/automobiles'
            response = requests.get(url)
            content = json.loads(response.content)
            print(f"content: {content}")
            for automobile in content["autos"]: 
                AutomobileVO.objects.update_or_create(
                    import_href=automobile["href"], 
                    defaults = {"color": automobile["color"], 
                                "year": automobile["year"], 
                                "vin": automobile["vin"],
                                "sold": automobile["sold"],  
                            }
                )
        except requests.exceptions.RequestException as e:
            print("Request Error:")
            print(e, file=sys.stderr)
        except Exception as e:
            print("ERROR OCCURED...")
            print(e, file=sys.stderr)

        print("Complete will run again in two seconds.")
        time.sleep(2)

if __name__ == "__main__":
    poll()
