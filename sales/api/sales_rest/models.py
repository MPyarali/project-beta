from django.db import models

# Create your models here.
class AutomobileVO(models.Model): 
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self): 
        return f"Automobile (color: {self.color} year: {self.year} vin: {self.vin} sold: {self.sold} import_href: {self.import_href})"

class Salesperson(models.Model): 
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)

    def __str__(self): 
        return f"Salesperson (first_name: {self.first_name} last_name: {self.last_name} employee_id: {self.employee_id})"
    
class Customer(models.Model): 
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=300)
    phone_number = models.CharField(max_length=12)

    def __str__(self): 
        return f"Customer (first_name: {self.first_name} last_name: {self.last_name} address: {self.address} phone_number: {self.phone_number})"
    
class Sale(models.Model): 
    automobile = models.ForeignKey(
        AutomobileVO, 
        related_name = "sale", 
        on_delete = models.CASCADE, 
    )
    salesperson = models.ForeignKey(
        Salesperson, 
        related_name = "sale", 
        on_delete = models.CASCADE, 
    )
    customer = models.ForeignKey(
        Customer, 
        related_name = "sale", 
        on_delete = models.CASCADE, 
    )
    price = models.PositiveBigIntegerField() 


    def __str__(self): 
        return f"Sale (automobile: {self.automobile} salesperson:{self.salesperson} customer: {self.customer} price: {self.price})"
