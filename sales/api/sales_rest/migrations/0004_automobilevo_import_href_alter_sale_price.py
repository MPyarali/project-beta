# Generated by Django 4.0.3 on 2024-03-19 01:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0003_alter_sale_price'),
    ]

    operations = [
        migrations.AddField(
            model_name='automobilevo',
            name='import_href',
            field=models.CharField(max_length=200, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='sale',
            name='price',
            field=models.PositiveBigIntegerField(),
        ),
    ]
