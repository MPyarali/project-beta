from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Salesperson, Customer, AutomobileVO, Sale

# Create your views here.
class AutomobileVODetailEncoder(ModelEncoder): 
    model = AutomobileVO
    properties = [ 
        "vin",  
        "import_href", 
        "id",
    ]

class SalespersonDetailEncoder(ModelEncoder): 
    model = Salesperson
    properties = [
        "first_name", 
        "last_name", 
        "employee_id", 
        "id", 
    ]

class CustomerDetailEncoder(ModelEncoder): 
    model = Customer
    properties = [
        "first_name", 
        "last_name", 
        "address", 
        "phone_number", 
        "id", 
    ]

class SaleDetailEncoder(ModelEncoder): 
    model = Sale
    properties = [
        "automobile", 
        "salesperson", 
        "customer", 
        "price", 
        "id", 
    ]
    encoders = {
        'salesperson': SalespersonDetailEncoder(), 
        'automobile': AutomobileVODetailEncoder(), 
        'customer': CustomerDetailEncoder(), 
    }

@require_http_methods(["GET", "POST"])
def api_list_salespersons(request): 
    if request.method == "GET": 
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson}, 
            encoder = SalespersonDetailEncoder
        )
    else: 
        try: 
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson, 
                encoder=SalespersonDetailEncoder, 
                safe=False
            )
        except: 
            response = JsonResponse(
                {"message": "Could not create this new salesperson"}
            )
            response.status_code = 400
            return response


    
@require_http_methods(["GET", "DELETE"])
def api_show_salesperson(request, pk): 
   if request.method == "DELETE": 
        count, _ = Salesperson.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
   else: 
        try: 
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salesperson, 
                encoder= SalespersonDetailEncoder, 
                safe=False
            ) 
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

   
@require_http_methods(["GET", "POST"])
def api_list_customers(request): 
    if request.method == "GET": 
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer}, 
            encoder = CustomerDetailEncoder
        )
    else: 
        try: 
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer, 
                encoder=CustomerDetailEncoder, 
                safe=False
            )
        except: 
            response = JsonResponse(
                {"message": "Could not create this new customer"}
            )
            response.status_code = 400
            return response
    
@require_http_methods(["GET", "DELETE"])
def api_show_customer(request, pk): 
   if request.method == "DELETE": 
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
   else: 
        try: 
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer, 
                encoder= CustomerDetailEncoder, 
                safe=False
            )
        except Customer.DoesNotExist:
                response = JsonResponse({"message": "Does not exist"})
                response.status_code = 404
                return response
   
@require_http_methods(["GET", "POST"])
def api_list_sales(request): 
    if request.method == "GET": 
        sale = Sale.objects.all()
        print(f"Sale {sale}")
        return JsonResponse(
            {"sales": sale}, 
            encoder = SaleDetailEncoder, 
            safe=False
        )
    else: 
        try: 
            content = json.loads(request.body)
            automobile_id = content["automobile_id"]
            salesperson_id = content["salesperson_id"]
            customer_id = content["customer_id"]
            automobile = AutomobileVO.objects.get(pk=automobile_id)
            customer = Customer.objects.get(pk=customer_id)
            salesperson = Salesperson.objects.get(pk=salesperson_id)
            content["automobile"] = automobile
            content["customer"]= customer
            content["salesperson"]= salesperson
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale, 
                encoder=SaleDetailEncoder, 
                safe=False
            )
        except: 
            response = JsonResponse(
                {"message": "Could not create this new sale"}
            )
            response.status_code = 400
            return response
        
@require_http_methods(["GET", "DELETE"])
def api_show_sale(request, pk): 
   if request.method == "DELETE": 
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
   else: 
        try: 
            sale = Sale.objects.get(id=pk)
            return JsonResponse(
                sale, 
                encoder= SaleDetailEncoder, 
                safe=False
            ) 
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


def list_automobiles(request): 
    automobiles = AutomobileVO.objects.all() 
    return JsonResponse(
        automobiles, 
        encoder = AutomobileVODetailEncoder, 
        safe=False
    )

def show_automobile_details(request, vin): 
    try: 
        automobile = AutomobileVO.objects.get(vin=vin)
        return JsonResponse(
            automobile, 
            encoder= AutomobileVODetailEncoder, 
            safe=False
        )
    except AutomobileVO.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response