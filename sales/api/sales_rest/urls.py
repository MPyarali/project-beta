from django.urls import path
from .views import api_list_salespersons, api_show_salesperson, api_list_customers, api_show_customer, list_automobiles, show_automobile_details, api_list_sales, api_show_sale

urlpatterns = [
    path("salespeople/", api_list_salespersons, name="api_list_salespeople"),
    path("salespeople/<int:pk>/", api_show_salesperson, name="api_show_salesperson"),
    path("customers/", api_list_customers, name="api_list_customers"), 
    path("customers/<int:pk>/", api_show_customer, name="api_show_customer"), 
    path("automobile/", list_automobiles, name="show_automobiles"), 
    path("automobile/<str:vin>/", show_automobile_details, name= "show_automobile_details"), 
    path("sales/", api_list_sales, name="api_list_sales"), 
    path("sales/<int:pk>/", api_show_sale, name="api_show_sales"), 

]